# Recetario y técnicas de cocina
## Conservas
 * [Dulce de Leche](conservas/dulce-de-leche.md)  
 * [Mermeladas de Frutas](conservas/mermelada-de-frutas.md)  
## Fermentación
 * [Masa Madre](fermentacion/masa-madre.md)  
 * [Vinagre de Sidra de Manzana](fermentacion/vinagre-manzana.md)  
## Panaderia
 * [Bagels](panaderia/bagels.md)  
 * [Baguette](panaderia/baguette.md)  
 * [Ciabatta](panaderia/ciabatta.md)  
 * [Pan Lactal Blanco](panaderia/pan-lactal-blanco.md)  
 * [Pan de Masa Madre](panaderia/pan-masa-madre.md)  
 * [Pan de Papa](panaderia/pan-papa.md)  
 * [Tapas de Tarta (no hojaldradas)](panaderia/tapas-tarta.md)  
 * [Tangzong (Roux de agua)](panaderia/tangzong)  
## Repostería
 * [Brownies](reposteria/brownies.md)  
 * [Chocolate Chip Cookies](reposteria/chocolate-chip-cookies.md)  
## Pastas
 * [Masa Base para Pastas](pastas/masa-base.md)  
### Rellenos
### Salsas
#### Tomates
#### Cremas
#### Emulsiones
## Carnes
### Rojas
### Cerdo
 * [Bondiola a la cerveza negra](carnes/cerdo/bondiola-cerveza.md)
### Pollo
 * [Pechugas a la crema](carnes/pollo/pollo-crema.md)
## Guarniciones

## Links útiles
### Videos
### Recetas online