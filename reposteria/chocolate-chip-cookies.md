# Chocolate Chip Cookies
_La receta está adaptada de una receta original Nortemaricana, los valores de cantidades están traducidos y aproximados de unidades imperiales a métricas._  

## Ingredientes
 | Ingrediente         | Cantidades |
 | ------------------- | ---------- |
 | Harina 0000         | 200g       |
 | Sal                 | 3g         |
 | Polvo para hornear  | 4g         |
 | Manteca (derretida) | 112g       |
 | Azucar rubia        | 200g       |
 | Azucar blanca       | 112g       |
 | Huevos              | 1 un.      |
 | Yema de huevo       | 1 un.      |
 | Esencia de vainilla | 6g         |
